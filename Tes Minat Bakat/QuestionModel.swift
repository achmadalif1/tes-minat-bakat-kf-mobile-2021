//
//  QuestionModel.swift
//  Tes Minat Bakat
//
//  Created by Ahmad Nur Alifullah on 21/10/21.
//

import Foundation

// MARK: - Welcome
struct Question: Codable {
    let headerKe: HeaderK
    let pertanyaanKe: [PertanyaanK]
    let headerKM: HeaderK
    let pertanyaanKM: [PertanyaanK]

    enum CodingKeys: String, CodingKey {
        case headerKe = "header_ke"
        case pertanyaanKe = "pertanyaan_ke"
        case headerKM = "header_km"
        case pertanyaanKM = "pertanyaan_km"
    }
}

// MARK: - HeaderK
struct HeaderK: Codable {
    let id, idKategori, namaModul, fassingGrade: String
    let totalSoal, waktuPengerjaan, type, doc: String
    let status: String

    enum CodingKeys: String, CodingKey {
        case id
        case idKategori = "id_kategori"
        case namaModul = "nama_modul"
        case fassingGrade = "fassing_grade"
        case totalSoal = "total_soal"
        case waktuPengerjaan = "waktu_pengerjaan"
        case type, doc, status
    }
}

// MARK: - PertanyaanK
struct PertanyaanK: Codable {
    let id, idSoalHeader, pertanyaan: String
    let pertanyaanImg, pathImg: String
    let idKunciJawaban, doc, status: String
    let jawaban: [Jawaban]

    enum CodingKeys: String, CodingKey {
        case id
        case idSoalHeader = "id_soal_header"
        case pertanyaan
        case pertanyaanImg = "pertanyaan_img"
        case pathImg = "path_img"
        case idKunciJawaban = "id_kunci_jawaban"
        case doc, status, jawaban
    }
}

// MARK: - Jawaban
struct Jawaban: Codable {
    let id, idPertanyaan: String
    let keyJawaban: KeyJawaban
    let namaJawaban, value: String
    let image, pathImage: String
    let doc, status: String

    enum CodingKeys: String, CodingKey {
        case id
        case idPertanyaan = "id_pertanyaan"
        case keyJawaban = "key_jawaban"
        case namaJawaban = "nama_jawaban"
        case value, image
        case pathImage = "path_image"
        case doc, status
    }
}

enum KeyJawaban: String, Codable {
    case a = "A"
    case b = "B"
    case c = "C"
    case d = "D"
}
