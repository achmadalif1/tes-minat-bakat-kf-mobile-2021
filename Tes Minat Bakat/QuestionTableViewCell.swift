//
//  QuestionTableViewCell.swift
//  Tes Minat Bakat
//
//  Created by Ahmad Nur Alifullah on 21/10/21.
//

import UIKit


class QuestionTableViewCell: UITableViewCell {
    @IBOutlet weak var answerChoice: UITableView!
    static let indentifier = "QuestionTableViewCell"
    var answer : [String] = ["1","2","3","4"]
    var choice : Int!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        answerChoice.register(AnswerTableViewCell.nib(), forCellReuseIdentifier: AnswerTableViewCell.indentifier)
        answerChoice.delegate = self
        answerChoice.dataSource = self
        
    }
    static func nib() -> UINib {
        return UINib(nibName: "QuestionTableViewCell", bundle: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension QuestionTableViewCell: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return answer.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: AnswerTableViewCell.indentifier, for: indexPath) as? AnswerTableViewCell{
            cell.label.text = answer[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
}
