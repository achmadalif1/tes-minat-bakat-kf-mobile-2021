//
//  ViewController.swift
//  Tes Minat Bakat
//
//  Created by Ahmad Nur Alifullah on 19/10/21.
//

import UIKit

class ViewController: UIViewController {
    let tesminatbakat = Bundle.main.decode(Question.self, from: "Question.json")
    var answer : [String] = ["1","2","3","4"]
    var choice : Int!
    var LJK_KE :[Int]!
    var LJK_KM :[Int]!
    var present = "KE"
    @IBOutlet weak var TableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        init_LJK_KE()
        init_LJK_KM()
        print(LJK_KE)
        print(LJK_KM)
        
        tableview_header()
        tableview_footer()
        
        TableView.register(AnswerTableViewCell.nib(), forCellReuseIdentifier: AnswerTableViewCell.indentifier)
        TableView.delegate = self
        TableView.dataSource = self
    }
    
    func init_LJK_KE (){
        if let ljk = UserDefaults.standard.object(forKey: "LJK_KE") as? [Int]{
            LJK_KE = ljk
        }else{
            if tesminatbakat.pertanyaanKe.count != nil && LJK_KE == nil {
                LJK_KE = Array(repeating: 5, count: tesminatbakat.pertanyaanKe.count)
            } else {
                print("Error data")
            }
        }
        
    }
    func init_LJK_KM (){
        if let ljk = UserDefaults.standard.object(forKey: "LJK_KM") as? [Int]{
            LJK_KM = ljk
        }else{
            if tesminatbakat.pertanyaanKe.count != nil && LJK_KM == nil {
                LJK_KM = Array(repeating: 5, count: tesminatbakat.pertanyaanKM.count)
            } else {
                print("Error data")
            }
        }
        
    }
    func tableview_header(){
        let customView =  UIView(frame: CGRect(x: 0,y: 0,width: TableView.frame.width,height: 40))
        customView.backgroundColor = .gray
        let titleLabel = UILabel(frame: CGRect(x:5,y: 0 ,width:customView.frame.width,height:40))
        if present == "KE"{
            titleLabel.text  = "Kecerdasan Emosional"
        }else{
            titleLabel.text  = "Kecerdasan Majemuk"
        }
        titleLabel.textColor = .black
        titleLabel.font = UIFont.systemFont(ofSize: 12)
        customView.addSubview(titleLabel)
        TableView.tableHeaderView = customView
    }
    func tableview_footer(){
        let customView =  UIView(frame: CGRect(x: 0,y: 0,width: TableView.frame.width,height: 30))
        customView.backgroundColor = .gray
        let nextButton = UIButton(frame: CGRect(x:0,y: 0 ,width:customView.frame.width/5,height:30))
        nextButton.setTitle("Next", for: .normal)
        nextButton.addTarget(self, action: #selector(nextButtonAction
                                                    ), for: .touchUpInside)
        let backButton = UIButton(frame: CGRect(x:0,y: 0 ,width:customView.frame.width/5,height:30))
        backButton.setTitle("Back", for: .normal)
        backButton.addTarget(self, action: #selector(backButtonAction
                                                    ), for: .touchUpInside)
        if present == "KE"{
            backButton.isEnabled = false
    
        }else{
            nextButton.isEnabled = false
    
        }
        let stackview = UIStackView(arrangedSubviews: [backButton,nextButton])
        stackview.frame = CGRect(x:0,y: 0 ,width:customView.frame.width,height:30)
        stackview.axis = .horizontal
        customView.addSubview(stackview)
        stackview.distribution = .fillEqually
        stackview.setCustomSpacing(10, after: backButton)
        TableView.tableFooterView = customView
    }
    @objc func backButtonAction(sender: Any) {
       present = "KE"
        TableView.reloadData()
        tableview_footer()
    }
    @objc func nextButtonAction(sender: Any) {
       present = "KM"
        TableView.reloadData()
        tableview_footer()
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if present == "KE"{
            return "\(section + 1). \(tesminatbakat.pertanyaanKe[section].pertanyaan)"
        }else{
            return "\(section + 1). \(tesminatbakat.pertanyaanKM[section].pertanyaan)"
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if present == "KE"{
            return tesminatbakat.pertanyaanKe.count
        }else{
            return tesminatbakat.pertanyaanKM.count
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: AnswerTableViewCell.indentifier, for: indexPath) as? AnswerTableViewCell{
            if present == "KE"{
                cell.label.text = tesminatbakat.pertanyaanKe[indexPath.section].jawaban[indexPath.row].namaJawaban
                if (indexPath.row) == LJK_KE[indexPath.section]{
                    cell.button.image = UIImage(systemName: "circle.fill")
                }else{
                    cell.button.image = UIImage(systemName: "circle")
                }
                
            }else{
                cell.label.text = tesminatbakat.pertanyaanKM[indexPath.section].jawaban[indexPath.row].namaJawaban
                if (indexPath.row) == LJK_KM[indexPath.section]{
                    cell.button.image = UIImage(systemName: "circle.fill")
                }else{
                    cell.button.image = UIImage(systemName: "circle")
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tableView.deselectRow(at: indexPath, animated: true)
        if let cell = tableView.cellForRow(at: indexPath) as? AnswerTableViewCell{
            if present == "KE"{
                if (indexPath.row) != LJK_KE[indexPath.section]{
                    cell.button.image = UIImage(systemName: "circle.fill")
                    LJK_KE[indexPath.section] = indexPath.row
                    UserDefaults.standard.set(LJK_KE, forKey: "LJK_KE")
                }else{
                    cell.button.image = UIImage(systemName: "circle")
                    LJK_KE[indexPath.section] = 5
                    UserDefaults.standard.set(LJK_KE, forKey: "LJK_KE")
                }
                
            }else{
                if (indexPath.row) != LJK_KM[indexPath.section]{
                    cell.button.image = UIImage(systemName: "circle.fill")
                    LJK_KM[indexPath.section] = indexPath.row
                    UserDefaults.standard.set(LJK_KM, forKey: "LJK_KM")
                }else{
                    cell.button.image = UIImage(systemName: "circle")
                    LJK_KM[indexPath.section] = 5
                    UserDefaults.standard.set(LJK_KM, forKey: "LJK_KM")
                }
                
            }
            TableView.reloadData()
        }
    }
    
    
    
}
