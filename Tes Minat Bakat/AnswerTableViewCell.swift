//
//  AnswerTableViewCell.swift
//  Tes Minat Bakat
//
//  Created by Ahmad Nur Alifullah on 21/10/21.
//

import UIKit

class AnswerTableViewCell: UITableViewCell {
    static let indentifier = "AnswerTableViewCell"
    @IBOutlet weak var button: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    static func nib() -> UINib {
        return UINib(nibName: "AnswerTableViewCell", bundle: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
