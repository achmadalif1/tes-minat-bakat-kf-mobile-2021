//
//  Bundle-Decodable.swift
//  Test Vidio
//
//  Created by Ahmad Nur Alifullah on 01/08/21.
//

import Foundation
import UIKit

extension Bundle {
    func decode<T: Decodable>(_ type: T.Type, from file: String) -> T {
        guard let url = self.url(forResource: file, withExtension: nil) else {
            fatalError("Failed to locate \(file) in bundle.")
        }
        let data: Data
        do {
            data = try Data(contentsOf: url) //<-`try`, not `try?`
        } catch {
            fatalError("Failed to load \(file) from bundle.: \(error)")
        }
            

        let decoder = JSONDecoder()

        let loaded: T
        do {
            loaded = try decoder.decode(T.self, from: data) //<-`try`, not `try?`
        } catch {
            fatalError("Failed to decode \(file) from bundle.: \(error)")
        }
                

        return loaded
    }
}
